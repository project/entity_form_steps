<?php

namespace Drupal\entity_form_steps\Form;

use Drupal\Component\Utility\SortArray;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;

/**
 * Provides method to enable steps using field groups in a form mode.
 */
class EntityFormSteps {

  /**
   * Get entity form steps.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   Returns an array of ordered steps.
   */
  public static function getSteps(FormStateInterface $form_state): array {
    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $formDisplay */
    $formDisplay = $form_state->getStorage()['form_display'];

    $steps = array_filter($formDisplay->getThirdPartySettings('field_group'), function ($step) {
      return $step['format_type'] === 'steps' && $step['region'] === 'content';
    });
    foreach ($steps as &$step) {
      $step['label'] = Xss::filterAdmin($step['label']);
      array_walk($step['format_settings'], function (&$value): void {
        if (is_string($value)) {
          $value = Xss::filterAdmin($value);
        }
      });
    }
    uasort($steps, [SortArray::class, 'sortByWeightElement']);

    if ($steps) {
      $entity = $form_state->getFormObject()->getEntity();
      // Invoke hook_entity_form_steps_alter().
      \Drupal::moduleHandler()->invokeAll(
        "entity_form_steps_alter",
        [&$steps, $form_state, $entity]
      );
      // Invoke hook_entity_ENTITY_TYPE_form_steps_alter().
      \Drupal::moduleHandler()->invokeAll(
        "entity_{$entity->getEntityTypeId()}_form_steps_alter",
        [&$steps, $form_state, $entity]
      );
      // Invoke hook_entity_ENTITY_TYPE_form_BUNDLE_steps_alter().
      \Drupal::moduleHandler()->invokeAll(
        "entity_{$entity->getEntityTypeId()}_form_{$entity->bundle()}_steps_alter",
        [&$steps, $form_state, $entity]
      );
    }

    return $steps;
  }

  /**
   * Alter entity form to initialize entity form steps.
   *
   * Steps are defined as field group formatters on the form mode. Create
   * the field group steps and use drag-n-drop to place fields.
   *
   * @see \Drupal\entity_form_steps\Plugin\field_group\FieldGroupFormatter\Step
   */
  public static function alterForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\Core\Entity\ContentEntityForm $formObject */
    $formObject = $form_state->getFormObject();
    if ($formObject->getOperation() === 'delete') {
      return;
    }
    if (!($entity = $formObject->getEntity())->isDefaultTranslation()) {
      return;
    }

    // If the property exists it can be assumed values are unsaved. This
    // does nothing on its own. One usage example is to warn users of
    // unsaved changes before navigating away from the form.
    if ($form_state->has('entity_form_steps')) {
      $form['#attributes']['data-unsaved'] = TRUE;
    }

    // Tell form state about the entity form steps.
    if (!($state = $form_state->get('entity_form_steps'))) {
      if (!($steps = static::getSteps($form_state))) {
        return;
      }
      $step = key($steps);
      $state = [
        'steps' => $steps,
        'current_step' => $step,
        'start' => $step === array_key_first($steps),
        'complete' => $step === array_key_last($steps),
      ];
      $form_state->set('entity_form_steps', $state);
    }
    if (!($step = $state['steps'][$state['current_step']] ?? [])) {
      return;
    }

    array_unshift($form['#validate'], [
      static::class, 'validateForm',
    ]);
    $form['#validate'][] = [
      static::class, 'validatePreviousForm',
    ];
    array_unshift($form['actions']['submit']['#submit'], [
      static::class, 'submitForm',
    ]);

    $form['actions']['submit']['#limit_validation_errors'] = [];
    foreach (array_keys($state['steps']) as $stepName) {
      // Hide inactive steps by setting access property. Must be set on
      // fields within the step to avoid false-positive validation errors
      // and prevent loss of values between steps.
      if ($stepName !== $state['current_step']) {
        $form[$stepName]['#access'] = FALSE;
        if (isset($form['#fieldgroups'][$stepName])) {
          static::setAccess($stepName, $form);
        }
      }

      // Limit validation errors to elements on the current step.
      elseif (isset($form['#fieldgroups'][$stepName])) {
        if ($entity instanceof FieldableEntityInterface && $entity->hasField('created')) {
          // Allow created value to be set regardless of current step.
          $form['actions']['submit']['#limit_validation_errors'][] = ['created'];
        }
        static::setValidation($form['#fieldgroups'][$stepName]->children, $form, $form);
      }
    }

    // Update the form title and action buttons.
    if ($title = Markup::create($step['format_settings'][$entity->isNew() ? 'add_label' : 'edit_label'])) {
      $form['#title'] = $title;
    }
    if ($state['start'] && $step['format_settings']['cancel_button']) {
      $form['actions']['cancel_button'] = [
        '#type' => 'link',
        '#title' => $step['format_settings']['cancel_button'],
        '#attributes' => ['class' => ['button']],
        '#url' => static::getCancelUrl($state, $form_state),
        '#cache' => [
          'contexts' => [
            'url.query_args:destination',
          ],
        ],
      ];
    }
    if (!$state['start'] && $step['format_settings']['previous_button']) {
      $form['actions']['previous_button'] = [
        '#type' => 'submit',
        '#value' => $step['format_settings']['previous_button'],
        '#name' => 'entity_form_steps_previous',
        '#submit' => [[static::class, 'submitForm']],
        '#limit_validation_errors' => $form['actions']['submit']['#limit_validation_errors'] ?? [],
      ];
    }
    if (!$state['complete'] && $step['format_settings']['next_button']) {
      $form['actions']['submit']['#value'] = $step['format_settings']['next_button'];
    }
    elseif ($step['format_settings']['submit_button'] && isset($form['actions']['submit'])) {
      $form['actions']['submit']['#value'] = $step['format_settings']['submit_button'];
    }
    if (isset($form['actions']['preview'])) {
      if ($step['format_settings']['preview_button']) {
        $form['actions']['preview']['#value'] = $step['format_settings']['preview_button'];
      }
      else {
        unset($form['actions']['preview']);
      }
    }
    if (isset($form['actions']['delete'])) {
      if ($step['format_settings']['delete_path']) {
        $url = \Drupal::token()->replace($step['format_settings']['delete_path'], [
          $entity->getEntityTypeId() => $entity,
        ]);
        if (UrlHelper::isValid($url, TRUE)) {
          $form['actions']['delete']['#url'] = Url::fromUri($url);
        }
        else {
          $form['actions']['delete']['#url'] = Url::fromUserInput($url);
        }
      }
      if ($step['format_settings']['delete_button']) {
        $form['actions']['delete']['#title'] = $step['format_settings']['delete_button'];
      }
      else {
        unset($form['actions']['delete']);
      }
    }

    // Invoke hook_entity_form_steps_complete_form_alter().
    \Drupal::moduleHandler()->invokeAll(
      "entity_form_steps_complete_form_alter",
      [&$form, $form_state, $entity, $state]
    );
    // Invoke hook_entity_ENTITY_TYPE_form_steps_complete_form_alter().
    \Drupal::moduleHandler()->invokeAll(
      "entity_{$entity->getEntityTypeId()}_form_steps_complete_form_alter",
      [&$form, $form_state, $entity, $state]
    );
    // Invoke hook_entity_ENTITY_TYPE_form_BUNDLE_steps_complete_form_alter().
    \Drupal::moduleHandler()->invokeAll(
      "entity_{$entity->getEntityTypeId()}_form_{$entity->bundle()}_steps_complete_form_alter",
      [&$form, $form_state, $entity, $state]
    );
  }

  /**
   * Set access on group elements.
   *
   * @param string $groupName
   *   The field group name.
   * @param array $form
   *   The form.
   */
  public static function setAccess(string $groupName, array &$form): void {
    if (isset($form['#fieldgroups'][$groupName])) {
      foreach ($form['#fieldgroups'][$groupName]->children as $element) {
        if (str_starts_with($element, 'group_') && isset($form['#fieldgroups'][$element])) {
          // Recursively set access on nested field groups.
          static::setAccess($element, $form);
        }
        $form[$element]['#access'] = FALSE;
      }
    }
  }

  /**
   * Set fields to validate for the current step.
   *
   * @param array $keys
   *   The element keys.
   * @param array $form
   *   The form.
   * @param array $element
   *   The element.
   * @param array $tree
   *   The parent element keys.
   */
  public static function setValidation(array $keys, array &$form, array $element, array $tree = []): void {
    foreach ($keys as $key) {
      if (isset($form['#fieldgroups'][$key])) {
        static::setValidation($form['#fieldgroups'][$key]->children, $form, $element, $tree);
      }
      if (!isset($element[$key]) || $key === 'widget') {
        continue;
      }
      $form['actions']['submit']['#limit_validation_errors'][] = array_merge($tree, [$key]);

      if ($children = Element::children($element[$key])) {
        $parents = isset($element[$key]['#tree']) && $element[$key]['#tree'] ? array_merge($tree, [$key]) : [];
        static::setValidation($children, $form, $element[$key], $parents);
      }
    }
  }

  /**
   * Get cancel button URL.
   *
   * @param array $state
   *   The steps state.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Url
   *   Returns a URL.
   */
  public static function getCancelUrl(array $state, FormStateInterface $form_state): Url {
    $entity = $form_state->getFormObject()->getEntity();
    $step = $state['steps'][$state['current_step']];

    // Redirect to specified route if configured.
    if ($step['format_settings']['cancel_path']) {
      $url = \Drupal::token()->replace($step['format_settings']['cancel_path'], [
        $entity->getEntityTypeId() => $entity,
      ]);
      if (UrlHelper::isValid($url, TRUE)) {
        return Url::fromUri($url);
      }
      return Url::fromUserInput($url);
    }
    // Redirect to the existing entity canonical route.
    elseif ($entity->id()) {
      return Url::fromRoute("entity.{$entity->getEntityTypeId()}.canonical", [
        $entity->getEntityTypeId() => $entity->id(),
      ]);
    }

    // Redirect to current user canonical route if new entity.
    return Url::fromRoute('entity.user.canonical', [
      'user' => \Drupal::currentUser()->id(),
    ]);
  }

  /**
   * Validate handler to manage steps and rebuild form.
   */
  public static function validateForm(array $form, FormStateInterface $form_state): void {
    /** @var \Drupal\Core\Entity\EntityForm $formObject */
    $formObject = $form_state->getFormObject();
    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    $entity = $formObject->getEntity();

    $form_state->setRebuild();

    // Remove values not on the current step and build entity. Necessary to
    // support elements that are not using field widget plugins. Such as
    // custom fields added on the form build array.
    $triggeringElement = $form_state->getTriggeringElement();
    if (!empty($triggeringElement['#limit_validation_errors'])) {
      $values = $form_state->getValues();
      $keys = $form['actions']['submit']['#limit_validation_errors'];
      array_walk($keys, function (&$keys): void {
        $keys = current($keys);
      });
      foreach (array_keys($values) as $key) {
        if ($entity->hasField($key) && !in_array($key, $keys, TRUE)) {
          unset($values[$key]);
        }
      }
      $form_state->setValues($values);
      $entity = $formObject->buildEntity($form, $form_state);
      $formObject->setEntity($entity);
    }

    // Set internal array pointer to current step.
    $state = $form_state->get('entity_form_steps');
    reset($state['steps']);
    foreach (array_keys($state['steps']) as $stepName) {
      if ($stepName !== $state['current_step']) {
        next($state['steps']);
      }
      else {
        break;
      }
    }

    // Invoke hook_entity_form_steps_state_alter().
    \Drupal::moduleHandler()->alter(
      "entity_form_steps_state",
      $state,
      $form_state,
      $entity
    );
    // Invoke hook_entity_ENTITY_TYPE_form_steps_state_alter().
    \Drupal::moduleHandler()->alter(
      "entity_{$entity->getEntityTypeId()}_form_steps_state",
      $state,
      $form_state,
      $entity
    );
    // Invoke hook_entity_ENTITY_TYPE_form_BUNDLE_steps_state_alter().
    \Drupal::moduleHandler()->alter(
      "entity_{$entity->getEntityTypeId()}_form_{$entity->bundle()}_steps_state",
      $state,
      $form_state,
      $entity
    );

    switch ($form_state->getTriggeringElement()['#name']) {
      case 'entity_form_steps_previous':
        // Go to the previous step.
        prev($state['steps']);
        break;

      case 'op':
        if (!$state['complete']) {
          // Go to the next step.
          next($state['steps']);
        }
        else {
          // Release form to the entity builder.
          $form_state->setRebuild(FALSE);
        }
        break;

      default:
        // Release form to the submitter. Necessary to allow other triggering
        // elements, like 'Add another item' buttons, to rebuild the form.
        $form_state->setRebuild(FALSE);
        break;
    }

    $state['current_step'] = key($state['steps']);
    $state['start'] = $state['current_step'] === array_key_first($state['steps']);
    $state['complete'] = $state['current_step'] === array_key_last($state['steps']);
    $form_state->set('entity_form_steps', $state);
  }

  /**
   * Validate handler to clear errors with previous button.
   */
  public static function validatePreviousForm(array $form, FormStateInterface $form_state): void {
    if ($form_state->getTriggeringElement()['#name'] !== 'entity_form_steps_previous' || !$errors = $form_state->getErrors()) {
      return;
    }
    /** @var \Drupal\Core\Entity\EntityForm $formObject */
    $formObject = $form_state->getFormObject();
    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    $entity = $formObject->getEntity();
    /** @var \Drupal\Core\Entity\FieldableEntityInterface $original */
    $original = !$entity->isNew() ? \Drupal::entityTypeManager()->getStorage($entity->getEntityTypeId())->loadUnchanged($entity->id()) : NULL;

    // Revert or remove erroneous values whilst preserving valid values.
    foreach ($errors as $key => $error) {
      if (($field = current(explode('][', $key))) && $entity->hasField($field)) {
        $entity->set($field, $original ? $original->get($field)->getValue() : NULL);
      }
    }
    $form_state->clearErrors();
  }

  /**
   * Submit handler to massage values before saving.
   */
  public static function submitForm(array $form, FormStateInterface $form_state): void {
    // Steps are only supported on the default translation form. Empty value
    // to prevent the content translation handler from changing the author,
    // created timestamp, and potentially other fields.
    $form_state->setValue('content_translation', []);
  }

}
