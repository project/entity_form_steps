# Entity Form Steps

Create multistep forms using field groups on entity form displays. Easy setup and configuration! Fully compatible
with any configurable form display mode and entity type. Customizable buttons and labels for each step. Additional
customization opportunities using the available hooks.

## Caveats

1. Multistep forms are only supported on the default translation entity form.
2. User account forms depend on the https://www.drupal.org/project/drupal/issues/3328962 patch.

## Dependencies

- [Field Group](https://www.drupal.org/project/field_group)

## Usage

1. Download and install the `drupal/entity_form_steps` module. Recommended install method is composer:
   ```
   composer require drupal/entity_form_steps
   ```
2. Go to the "Manage form display" tab of the desired entity type.
3. Click "Add field group" on the desired form mode. Select "Form step" in the dropdown.
4. Review the available configurations and create the group.
5. Use the drag-and-drop table rows to place fields into the form step field group.
6. Repeat as many times as needed.
